DROP DATABASE pythontest;
CREATE DATABASE pythontest;

USE pythontest;

CREATE TABLE user (
    id VARCHAR(36) PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL UNIQUE,
    status VARCHAR(128) DEFAULT 'active'
);

INSERT INTO user VALUES 
    ('036d0166-ef9c-4cda-85f1-7769bc756b91', 'Peter Watts', 'pwatts@millennium.com', 'active'),
    ('44631bb5-e2fe-4cee-95e0-4c08ad2f4c80', 'Frank Black', 'fblack@millennium.com', 'active'),
    ('16161cf3-3b99-4281-bcdf-8fb15d2cf44e', 'Emma Hollis', 'ehollis@millennium.com', 'active'),
    ('b41bfc8c-ff1a-4903-9683-51af26769772', 'Lara Means', 'lmeans@millennium.com', 'inactive');

CREATE TABLE api_user (
    api_key VARCHAR(36) PRIMARY KEY,
    user_id VARCHAR(36) NOT NULL UNIQUE,
    access_type VARCHAR(20) NOT NULL DEFAULT 'ro'
);

INSERT INTO api_user VALUES
    ('ffc0199a-e2b5-4f45-a5d3-bb3d9b14df32', '44631bb5-e2fe-4cee-95e0-4c08ad2f4c80', 'ro'),
    ('0fadc495-59e5-4eff-8de1-b2f39a285403', '16161cf3-3b99-4281-bcdf-8fb15d2cf44e', 'rw');
