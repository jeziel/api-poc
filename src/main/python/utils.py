
import os, json


dir_name = os.path.dirname(os.path.abspath(__file__))

def get_schema(schema_file):
    schema_dir = os.path.abspath(os.path.join(dir_name, f'../schemas/'))
    with open(f'{schema_dir}/{schema_file}', 'r') as fp:
        data = json.load(fp)

    return data
