
from database.mysql import MySql


class ApiUser(object):
    access_types = ['ro', 'rw']

    def __init__(self, key, user_id, access_type):
        self.key = key
        self.user_id = user_id
        self.access_type = access_type

    @staticmethod
    def get(api_key: str):
        if not api_key:
            return None

        with MySql() as connection:
            # TODO. active?
            query = "SELECT user_id, access_type FROM api_user WHERE api_key=%(api_key)s"
            params = {'api_key': api_key}

            api_user = connection.select_one(query, params)

            if not api_user:
                return None

            (user_id, access_type) = api_user

            return ApiUser(api_key, user_id, access_type)

    def has_access(self, access):
        return access == 'ro' or self.access_type == access
