
import json
import logging
import uuid
from typing import Dict

from jsonschema import validate
from jsonschema.exceptions import ValidationError

from database.mysql import MySql
from database.exceptions import UnableToCreateEntity, InvalidEntity
from utils import get_schema


# TODO. Use https://docs.python.org/3.4/library/enum.html ?
class UserStatus(object):
    active = 'active'
    inactive = 'inactive'


class User(object):
    _entity = 'user'

    def __init__(self, user_id, name, email, status):
        self.user_id = user_id
        self.name = name
        self.email = email
        self.status = status

    @staticmethod
    def get(user_id: str):
        with MySql() as connection:
            query = 'SELECT id, name, email, status FROM user WHERE id = %(user_id)s'
            params = {'user_id': user_id}
            user = connection.select_one(query, params)

            if not user:
                return None

            (user_id, name, email, status) = user

            return User(user_id, name, email, status)

    @staticmethod
    def create(entity: Dict):
        try:
            schema = get_schema('user.json')
            validate(instance=entity, schema=schema)

            with MySql() as connection:
                user_uuid = str(uuid.uuid4())
                query = 'INSERT INTO user (id, name, email) VALUES (%(user_uuid)s, %(name)s, %(email)s)'
                params = {
                    'user_uuid': user_uuid,
                    'name': entity.get('name'),
                    'email': entity.get('email'),
                }

                connection.insert(query, params)

                return User(user_uuid, entity.get('name'), entity.get('email'), UserStatus.active)
        except ValidationError as e:
            logging.warning('Unable to create user', exc_info=True)
            raise InvalidEntity('validation', User._entity)
        except Exception as e:
            logging.warning('Unable to create user', exc_info=True)
            raise UnableToCreateEntity(str(e), User._entity)

    def delete(user_id: int):
        with MySql() as connection:
            query = 'DELETE FROM user WHERE id = %(user_id)s'
            params = {'user_id': user_id}
            connection.delete(query, params)

            logging.info(f'User {user_id} deleted')

            return True

    def is_active(self):
        return self.status == UserStatus.active

    def activate(self):
        return self._change_user_status(UserStatus.active)

    def deactivate(self):
        return self._change_user_status(UserStatus.inactive)

    def as_dict(self):
        return {
            'id': self.user_id,
            'name': self.name,
            'email': self.email,
            'status': self.status,
        }

    def as_json(self):
        return json.dumps(self.as_dict())

    def _change_user_status(self, user_status: str):
        with MySql() as connection:
            query = 'UPDATE user SET status = %(user_status)s WHERE id=%(user_id)s'
            params = {'user_id': self.user_id, 'user_status': user_status}

            connection.update(query, params)


