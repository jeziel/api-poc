
import functools
import logging

from flask import request

from api.standard_responses import Standards
from models.api_user import ApiUser


def validate_access(access_required):
    def validate_access(f):
        @functools.wraps(f)
        def check_key(*args, **kwargs):
            api_key = request.headers.get('x-api-key')
            api_user = ApiUser.get(api_key)

            if not api_user:
                logging.warning(f'No api user for key: {api_key}')
                return Standards.bad_credentials()

            if not api_user.has_access(access_required):
                logging.warning(f'No access ({access_required}) for key: {api_key}')
                return Standards.no_access()

            return f(*args, **kwargs)

        return check_key

    return validate_access
