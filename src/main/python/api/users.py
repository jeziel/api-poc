
from flask import request, Blueprint
import json
import logging

from models.user import User
from database.exceptions import InvalidEntity, UnableToCreateEntity
from decorators import validate_access
from api.standard_responses import Standards


bp = Blueprint('user', __name__, url_prefix='/user')


@bp.route('/', methods=['POST'])
@validate_access('rw')
def create_user():
    try:
        user_data = request.get_json()
        user = User.create(user_data)

        return Standards.created(user.as_dict())
    except InvalidEntity as e:
        return Standards.bad_request(e)
    except UnableToCreateEntity as e:
        return Standards.internal_error(e)


@bp.route('/<string:user_id>')
@validate_access('ro')
def get_user(user_id: str):
    user = User.get(user_id)

    if not (user and user.is_active()):
        return Standards.not_found()

    return Standards.ok(user.as_dict())


@bp.route('/<string:user_id>/deactivate', methods=['PUT'])
@validate_access('rw')
def deactivate_user(user_id: str):
    user = User.get(user_id)

    if not user:
        return Standards.not_found()

    if user.is_active():
        user.deactivate()

    return Standards.no_content()


@bp.route('/<string:user_id>/activate', methods=['PUT'])
@validate_access('rw')
def activate_user(user_id: str):
    user = User.get(user_id)

    if not user:
        return Stardards.not_found()

    if not user.is_active():
        user.activate()

    return Standards.no_content()

