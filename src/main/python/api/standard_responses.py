

class Standards(object):
    _not_found = {'message': 'not found'}
    _created = {'message': 'created'}
    _ok = {'message': 'ok'}
    _credentials = {'message': 'invalid key'}
    _no_access = {'message': 'invalid access'}

    @staticmethod
    def ok(body=None):
        payload = Standards._ok if not body else body
        return payload, 200

    @staticmethod
    def created(body=None):
        payload = Standards._created if not body else body
        return payload, 201

    @staticmethod
    def no_content():
        return '', 204

    @staticmethod
    def not_found(body=None):
        payload = Standards._not_found if not body else body
        return payload, 404

    @staticmethod
    def bad_request(e):
        body = e.as_payload()
        return body, 400

    @staticmethod
    def internal_error(e):
        payload = e.as_payload()
        return body, 500

    @staticmethod
    def bad_credentials():
        return Standards._credentials, 401

    @staticmethod
    def no_access():
        return Standards._no_access, 401
