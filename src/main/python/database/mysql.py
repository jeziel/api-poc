
import logging
import mysql.connector
from mysql.connector import errorcode
import sqlalchemy
from sqlalchemy import create_engine
import urllib.parse


config = {
    'user': 'pythontest',
    'password': 'secret',
    'host': 'db',
    'port': '3306',
    'database': 'pythontest'
}


class MySql(object):
    def __init__(self):
        self.connection = None
        self.cursor = None

    def __enter__(self):
        self.connection = mysql.connector.connect(**config)
        self.cursor = self.connection.cursor()

        return self
        # except mysql.connector.Error as err:
            # logging.error('Unable to connect to the database', exc_info=1)
        # except Exception as e:
            # logging.error('Unable to get a connection to the database')

    def __exit__(self, type, value, traceback):
        self.cursor.close()
        self.connection.close()

    def select_one(self, query, params):
        self._execute(query, params)

        return self.cursor.fetchone()

    def insert(self, query, params):
        self._commit(query, params)
        return self.cursor.lastrowid

    def delete(self, query, params):
        self._commit(query, params)

    def update(self, query, params):
        self._commit(query, params)

    def _execute(self, query, params):
        self.cursor.execute(query, params)

    def _commit(self, query, params):
        self._execute(query, params)
        self.connection.commit()


class SqlConnection(object):
    def __init__(self, dialect, driver, username, password, host, port, database):
        self.connection_string = ConnectionString(dialect, driver, username,
                                                  password, host, port, database)
        self._version = sqlalchemy.__version__
        self._engine = self._create_engine()
        # https://docs.sqlalchemy.org/en/13/dialects/mysql.html#connection-timeouts-and-disconnects
        self._pool_recycle_timeout = 3600

    def __enter__(self):
        pass

    def __exit__(self, type, value, traceback):
        pass

    def _create_engine(self):
        connection = str(self.connection_string)
        return create_engine(connection, pool_recycle=self._pool_recycle_timeout)


# https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
class ConnectionString(object):
    def __init__(self, dialect, driver, username, password, host, port, database):
        self.dialect = dialect
        self.driver = driver
        self.username = username
        self.password = password
        self.host = host
        self.port = port
        self.database = database

    def __str__(self):
        protocol = self._get_protocol()
        credentials = self._get_credentials()
        server = self._get_server()

        return f'{protocol}://{credentials}@{server}/{self.database}'

    def _get_protocol(self):
        if self.driver:
            return f'{self.dialect}+{self.driver}'

        return f'{self.dialect}'

    def _get_credentials(self):
        username = self.username
        password = urllib.parse.quote_plus(self.password)

        return f'{username}:{password}'

    def _get_server(self):
        if self.port:
            return f'{self.host}:{self.port}'

        return f'{self.host}'


class MySqlClientConnection(SqlConnection):
    """
    https://pypi.org/project/mysqlclient/
    """
    dialect = 'mysql'
    driver = 'mysqldb'

    def __init__(self, user_name, password, host, database):
        super(MySqlClientConnection, self).__init__(self.dialect, self.driver,
                                                    user_name, password, host,
                                                    None, database)

class PyMySqlConnection(object):
    """
    https://pymysql.readthedocs.io/en/latest/index.html
    """

    dialect = 'mysql'
    driver = 'pymysql'

    def __init__(self, user_name, password, host, database):
        super(PyMySqlConnection, self).__init__(self.dialect, self.driver,
                                                    user_name, password, host,
                                                    None, database)
