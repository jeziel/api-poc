
class UnableToCreateEntity(RuntimeError):
    def __init__(self, message, entity):
        super(UnableToCreateEntity, self).__init__(message)
        self.entity = entity

    def as_payload(self):
        return {
            'message': 'Unable to create entity',
            'entity': self.entity,
            'reason': str(self),
        }


class InvalidEntity(AssertionError):
    def __init__(self, message, entity):
        super(InvalidEntity, self).__init__(message)
        self.entity = entity

    def as_payload(self):
        return {
            'message': 'Unable to validate entity',
            'entity': self.entity,
            'reason': str(self),
        }

