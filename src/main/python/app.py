
from flask import Flask, request
from api.users import bp as users_bp


app = Flask(__name__)
app.register_blueprint(users_bp)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
