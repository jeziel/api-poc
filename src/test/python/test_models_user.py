
import pytest
import uuid
from models.user import User


skip = pytest.mark.skip


@skip
def test_get_returns_a_user_that_exists():
    user = User.get('036d0166-ef9c-4cda-85f1-7769bc756b91')
    assert user.as_dict() == {
        'id': '036d0166-ef9c-4cda-85f1-7769bc756b91',
        'name': 'Peter Watts',
        'email': 'pwatts@millennium.com'
    }
    assert user.is_active() == True


@skip
def test_get_returns_an_inactive_user():
    user = User.get('b41bfc8c-ff1a-4903-9683-51af26769772')
    assert user is not None
    assert user.as_dict() == {
        'id': 'b41bfc8c-ff1a-4903-9683-51af26769772',
        'name': 'Lara Means',
        'email': 'lmeans@millennium.com'
    }


def test_get_returns_none_when_the_user_does_not_exists():
    user = User.get('missing')
    assert user is None


def test_creates_adds_a_user(monkeypatch):
    user = _create_user(monkeypatch)
    assert user is not None
    _delete_user(user)


@skip
def test_deactivate_deactivates_a_user(monkeypatch):
    user = _create_user()
    assert user.is_active is True

    user.deactivate()
    assert user.is_active is False

    user2 = USer.get('u-u-i-d')
    assert user2 is not None
    assert user2.user_id == user.user_id
    assert user2.is_active is False

    _delete_user(user)


def _create_user(monkeypatch):
    monkeypatch.setattr(uuid, 'uuid4', lambda: 'u-u-i-d')

    user = User.create({
        'name': 'Unit Test',
        'email': 'unit-test@test.com',
    })

    assert user != None
    assert user.is_active() == True
    assert user.as_dict() == {
        'id': 'u-u-i-d',
        'name': 'Unit Test',
        'email': 'unit-test@test.com',
        'status': 'active',
    }

    return user

def _delete_user(user):
    # todo. I need to guarantee that this gets deleted even if the test fails.
    # unittest allows for teardown, but pytest? try:except?
    user_id = user.user_id

    assert user_id is not None
    User.delete(user_id)
