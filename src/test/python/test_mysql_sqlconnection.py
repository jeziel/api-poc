
import pytest
from database.mysql import SqlConnection, ConnectionString
import sqlalchemy
from sqlalchemy.exc import NoSuchModuleError


def test_init_should_have_the_version():
    sql_connection = SqlConnection('mysql', 'pymysql', 'u', 'p', 'h', None, 'd')
    assert '1.3.15' == sql_connection._version


def test_creating_a_connection_with_an_unknown_protocol_raises_an_exception():
    try:
        SqlConnection('d', 'dr', 'u', 'p', 'h', 100, 'd')
        pytest.fail('unknow protocol should have failed')
    except NoSuchModuleError as e:
        pass


def test_connection_string_initialized():
    connection_string = ConnectionString('d', 'dr', 'u', 'p', 'h', 100, 'd')
    assert connection_string.dialect == 'd'
    assert connection_string.driver == 'dr'
    assert connection_string.username == 'u'
    assert connection_string.password == 'p'
    assert connection_string.host == 'h'
    assert connection_string.port == 100
    assert connection_string.database == 'd'


def test_connection_string_has_a_string_representation():
    connection_string = ConnectionString('d', 'dr', 'u', 'p', 'h', 400, 'd')
    assert str(connection_string) == 'd+dr://u:p@h:400/d'


def test_connection_string_handle_optional_values():
    connection_string = ConnectionString('d', None, 'u', 'p', 'h', None, 'd')
    assert str(connection_string) == 'd://u:p@h/d'
