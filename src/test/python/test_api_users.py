import pytest


skip = pytest.mark.skip

ro_headers = {'x-api-key': 'ffc0199a-e2b5-4f45-a5d3-bb3d9b14df32'}
rw_headers = {'x-api-key': '0fadc495-59e5-4eff-8de1-b2f39a285403'}


def test_get_no_key_should_return_a_401(client):
    rv = client.get('/user/whatever')
    assert rv.status_code == 401
    assert rv.json == {'message': 'invalid key'}


@skip
def test_get_existing_user_should_return_the_user(client):
    rv = client.get('/user/036d0166-ef9c-4cda-85f1-7769bc756b91', headers=ro_headers)
    assert rv.status_code == 200
    assert rv.json == {
        'id': '036d0166-ef9c-4cda-85f1-7769bc756b91',
        'name': 'Peter Watts',
        'email': 'pwatts@millennium.com',
    }


def test_get_nonexistent_user_should_return_a_404(client):
    rv = client.get('/user/none', headers=ro_headers)
    assert rv.status_code == 404
    assert rv.json == {'message': 'not found'}


def test_deactivate_user_should_change_the_status(client):
    rv = client.get('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e', headers=rw_headers)
    assert rv.status_code == 200

    rv = client.put('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e/deactivate', headers=rw_headers)
    assert rv.status_code == 204

    rv = client.get('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e', headers=rw_headers)
    assert rv.status_code == 404

def test_deactivate_user_deactivating_a_deactivated_user_should_not_do_anything(client):
    rv = client.get('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e', headers=rw_headers)
    assert rv.status_code == 404

    rv = client.put('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e/deactivate', headers=rw_headers)
    assert rv.status_code == 204

    rv = client.get('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e', headers=rw_headers)
    assert rv.status_code == 404

def test_deactivate_user_with_a_ro_user_should_fail(client):
    rv = client.put('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e/deactivate', headers=ro_headers)
    assert rv.status_code == 401
    assert rv.json == {'message': 'invalid access'}

def test_deactivate_user_no_key_should_fail(client):
    rv = client.put('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e/deactivate')
    assert rv.status_code == 401
    assert rv.json == {'message': 'invalid key'}

def test_activate_user_should_change_the_status(client):
    rv = client.get('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e', headers=rw_headers)
    assert rv.status_code == 404

    rv = client.put('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e/activate', headers=rw_headers)
    assert rv.status_code == 204

    rv = client.get('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e', headers=rw_headers)
    assert rv.status_code == 200

def test_activating_user_activating_an_active_user_should_not_do_anything(client):
    rv = client.get('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e', headers=rw_headers)
    assert rv.status_code == 200

    rv = client.put('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e/activate', headers=rw_headers)
    assert rv.status_code == 204

    rv = client.get('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e', headers=rw_headers)
    assert rv.status_code == 200

def test_activate_user_with_a_ro_user_should_fail(client):
    rv = client.put('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e/activate', headers=ro_headers)
    assert rv.status_code == 401
    assert rv.json == {'message': 'invalid access'}

def test_activate_user_no_key_should_fail(client):
    rv = client.put('/user/16161cf3-3b99-4281-bcdf-8fb15d2cf44e/activate')
    assert rv.status_code == 401
    assert rv.json == {'message': 'invalid key'}

def xtest_create_user_a_valid_user_should_create(client):
    user_data = {
        'name': 'My Name',
        'email': 'my@email.com',
    }

    rv = client.post('/user/', json=user_data, headers=rw_headers)

    assert rv.status_code == 201
    assert rv.json == {
        'id': 'something',
        'name': 'My Name',
        'email': 'my@email.com',
        'active': True,
    }
