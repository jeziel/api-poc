FROM python:3.7-slim

ARG requirements_file=requirements.txt

#RUN apt-get update && apt-get install -y \
    #python3-dev \
    #default-libmysqlclient-dev

WORKDIR /usr/src/app
COPY requirements.txt test.sh ./
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait ./wait
RUN chmod +x ./wait

RUN pip install --no-cache-dir -r ./requirements.txt

