#!/usr/bin/env bash

cd /usr/src/app


echo "Validating OpenAPI spec."
openapi-spec-validator openapi/spec.yml

echo "Waiting for the DB."
./wait

echo "Running pytest."
pytest
